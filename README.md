# MyAva


Ce dépôt contient le matériel pédagogique pour le mini projet de géomorphologie enseigné à Sorbonne Université concernant les lois de friction des avalanches de roche et glissements de terrain.

- Des notes en français sur la modélisation sont fournies dans Note_ava.pdf.

- Les fichiers nécessaires à la simulation sont disponibles dans le répertoire simus.

- Quelques références se trouvent dans le répertoire Biblio.



Pour toutes questions:  A. Lucas, 2021 <lucas[~@~]ipgp.fr>

